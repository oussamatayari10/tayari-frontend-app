import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilMoney,
  cilPhone,
  cilSpeedometer,
  cilTag,
  cilUser,
} from '@coreui/icons'
import {  CNavItem } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'PurchaseBundle',
    to: '/purchaseBundle',
    icon: <CIcon icon={cilMoney} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'RechargeHRN',
    to: '/rechargeHRN',
    icon: <CIcon icon={cilTag} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'GetSubscriberInfo',
    to: '/getSubscriberInfo',
    icon: <CIcon icon={cilUser} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Ajouter Logs',
    to: '/logs',
    icon: <CIcon icon={cilPhone} customClassName="nav-icon" />,
  }
]

export default _nav
