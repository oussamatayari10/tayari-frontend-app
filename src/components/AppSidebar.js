import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { CSidebar, CSidebarBrand, CSidebarNav, CSidebarToggler } from '@coreui/react'
import logo1 from '../assets/images/ooredoo2.png'
import logo2 from '../assets/images/ooredoo.png'
import { AppSidebarNav } from './AppSidebarNav'


import SimpleBar from 'simplebar-react'
import 'simplebar/dist/simplebar.min.css'

// sidebar nav config
import navigation from '../_nav'
import navigationDigital from '../_navDigit'

const isDigital = ()=> {
  if (localStorage.getItem("role")==="1") return true
  else return false
}
console.log(localStorage.getItem("role"));
const AppSidebar = () => {
  const dispatch = useDispatch()
  const unfoldable = useSelector((state) => state.sidebarUnfoldable)
  const sidebarShow = useSelector((state) => state.sidebarShow)

  return (
    <CSidebar
      position="fixed"
      unfoldable={unfoldable}
      visible={sidebarShow}
      onVisibleChange={(visible) => {
        dispatch({ type: 'set', sidebarShow: visible })
      }}
    >
      <CSidebarBrand className="d-none d-md-flex" to="/">                  
        <img src={logo1} className="sidebar-brand-full" alt='ooredoo' width={"120px"} />
        <img src={logo2} className="sidebar-brand-narrow" alt='ooredoo' width={"50px"} />
      </CSidebarBrand>
      <CSidebarNav>
        <SimpleBar>
          {isDigital() ? (
          <AppSidebarNav items={navigationDigital} />
          ):(
            <AppSidebarNav items={navigation} />
          )}
        </SimpleBar>
      </CSidebarNav>
      <CSidebarToggler
        className="d-none d-lg-flex"
        onClick={() => dispatch({ type: 'set', sidebarUnfoldable: !unfoldable })}
      />
    </CSidebar>
  )
}

export default React.memo(AppSidebar)
