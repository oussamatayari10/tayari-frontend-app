import React, { Component } from 'react';
import axios from "axios";
import {
    CAlert,
    CButton,
    CCard,
    CCardBody,
    CCol,
    CContainer,
    CForm,
    CFormInput,
    CFormLabel,
    CRow,
    CSpinner,
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { cilCheckCircle } from '@coreui/icons';

const styleForm = {
    padding: "5rem 0"
}
const spanStyle ={
    display: 'flex', justifyContent: 'center', alignItems: 'center'
}
const token = localStorage.getItem('token');

export default class FormFile extends Component {

    constructor(props) {
        super(props);
        this.onFileChange = this.onFileChange.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            fileCollection: '',
            date: '',
            loaded: false,
            download : false,
        }
    }
    onFileChange(e) {
        console.log(e.target.files);
        this.setState({ fileCollection: e.target.files })
    }
    onDateChange = (e) => {
        this.setState({ date: e.target.value })
    }
    onSubmit(e) {
        e.preventDefault();
        this.download = true;
        var formData = new FormData();
        for (const key of Object.keys(this.state.fileCollection)) {
            formData.append('fileCollection', this.state.fileCollection[key])
        }
        formData.append('date', this.state.date)
        axios.post("http://localhost:8000/file/uploadLogs", formData, {headers: ({
            Authorization: 'Bearer ' + token
        })
        }).then(res => {
            this.setState({ loaded: true, date: '' })
            document.getElementById("formFile").value = "";
            console.log(this.state.loaded);
            console.log(res.data);
            this.download=false;
            setTimeout(() => {
                this.setState({ loaded: false })
            }, 3000);
        })
            .catch((err) => {
                console.log(err.message);
            })
    }
    render() {
        
        return (

            <div className="bg-light d-flex flex-row align-items-center " style={styleForm}>
                <CContainer>
                    <CAlert visible={this.state.loaded} color="success">
                    <CIcon icon={cilCheckCircle} className="flex-shrink-0 me-2" width={24} height={24} />
                        <span>
                            Vos logs sont importés
                        </span>
                    </CAlert>
                    <CRow className="justify-content-center ">
                        <CCol md={9} lg={7} xl={6}>
                            <CCard className="mx-4 text-white bg-danger">
                                <CCardBody className="p-4">
                                    <CForm
                                        className="row g-3"
                                        encType="multipart/form-data"
                                        onSubmit={this.onSubmit}
                                        id="form"
                                    >
                                        <CFormLabel htmlFor="formFile">Télécharger les Logs</CFormLabel>
                                        <CFormInput
                                            type="file"
                                            id="formFile"
                                            name="files"
                                            onChange={this.onFileChange}
                                            required
                                            multiple
                                        />
                                        <CFormInput
                                            type='date'
                                            name='date'
                                            value={this.state.date}
                                            onChange={this.onDateChange}
                                            required
                                        />
                                        <CButton type='submit' color="light" variant="outline" className="mt-4" tabIndex={-1}>
                                        <span style={spanStyle}>
                                            <CSpinner color="light" variant="grow"/>  
                                            Télécharger les Logs</span>
                                        </CButton>
                                    </CForm>
                                </CCardBody>
                            </CCard>
                        </CCol>
                    </CRow>
                </CContainer>
            </div>
        )
    }

}
