import React from 'react'
import {
  CButton,
  CDropdown,
} from '@coreui/react'
import {
  cilLockLocked,
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'

const clicked = () => {
  localStorage.removeItem('token')
  localStorage.removeItem('role')
  localStorage.removeItem('name')
  window.location.reload()
  window.location.assign('./#/login')
}
const AppHeaderDropdown = () => {
  return (
    <CDropdown variant="nav-item">
          
          <CButton onClick={clicked} color="danger" variant="outline" shape="rounded-pill">
            <CIcon icon={cilLockLocked} className="me-2" />
            SIGN OUT
          </CButton>
    </CDropdown>
  )
}

export default AppHeaderDropdown
