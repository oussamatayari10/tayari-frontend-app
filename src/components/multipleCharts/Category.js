import { CChartPie } from '@coreui/react-chartjs';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
/* eslint-disable react/prop-types */

const token = localStorage.getItem('token');

export default function Category(props) {

    const { dateLog } = props;
    const [names, setNames] = useState([]);
    const [nbTransaction, setNbTransaction] = useState([]);
    const [colors, setColors] = useState([]);
    const [hoverColors, setHoverColors] = useState([]);

    const style = {
        // "width": "35rem"
        textAlign: "center",
        color: "#494848",
    }

    useEffect(() => {
        props.setCategoryLoading(true)
        axios.get('http://localhost:8000/data/categories', {
            params: {
                date: dateLog
            },
            headers: {
                Authorization: "Bearer " + token,
            },
        })
            .then(async (resp) => {
                let couleurs = [];
                let hovers = [];
                for (let i = 0; i < resp.data.transaction.length; i++) {
                    const element = resp.data.transaction[i];
                    const nbTrans = nbTransaction;
                    const nameCategory = names;
                    nbTrans.push(element.count);
                    nameCategory.push(element._id.category)
                    setNbTransaction(nbTrans);
                    setNames(nameCategory);
                    couleurs.push("#" + Math.floor(Math.random() * 16777215).toString(16))
                    hovers.push("#" + Math.floor(Math.random() * 16777215).toString(16))
                    setColors(couleurs)
                    setHoverColors(hovers)
                    props.setCategoryLoading(false)
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])


    return (
        <>
            <h5 style={style}>Nombre de Transactions par Catégorie</h5>
            <CChartPie
                data={{
                    labels: names,
                    datasets: [
                        {
                            data: nbTransaction,
                            backgroundColor: colors,
                            hoverBackgroundColor: hoverColors,
                        },
                    ],
                }}
            /></>)
}