import { cilHappy, cilPeople } from '@coreui/icons';
import CIcon from '@coreui/icons-react';
import { CCol, CRow, CWidgetStatsC } from '@coreui/react';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
/* eslint-disable react/prop-types */

const token = localStorage.getItem('token');

export default function Clients(props) {

    const { dateLog } = props;
    const [nbClients, setNbClients] = useState(0);
    const [nbSuccessStatus, setNbSuccessStatus] = useState(0);

    useEffect(() => {
        props.setClientLoading(true)
        axios.get('http://localhost:8000/data/clients', {
            params: {
                date: dateLog
            }
            ,
            headers: {
                Authorization: "Bearer " + token,
            },
        })
            .then(async (resp) => {
                setNbClients(resp.data);
                props.setClientLoading(false)
            })
            .catch((err) => {
                console.log(err.message);
                props.setClientLoading(false)
            })

        axios.get('http://localhost:8000/data/status',
            {
                params: {
                    date: dateLog
                },
                headers: {
                    Authorization: "Bearer " + token,
                },
            })
            .then(async (resp) => {
                setNbSuccessStatus(resp.data);
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])




    return (
        <>
            <CRow>
                <CCol>
                    <CWidgetStatsC
                        icon={<CIcon icon={cilPeople} height={36} />}
                        value={nbClients.distinctCount}
                        title="Utilisateurs"
                        progress={{ color: 'danger', value: nbClients.distinctCount }}
                        className="mb-4"
                    />
                </CCol>
                <CCol>
                    <CWidgetStatsC
                        icon={<CIcon icon={cilHappy} height={36} />}
                        value={nbSuccessStatus.count}
                        title="Transactions réussites (%)"
                        progress={{ color: 'success', value: nbSuccessStatus.count }}
                        className="mb-4"
                    />
                </CCol>
            </CRow>
        </>
    )
}