import { CChart } from '@coreui/react-chartjs';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
/* eslint-disable react/prop-types */

const token = localStorage.getItem('token');

export default function Latency(props) {
    const { dateLog } = props;

    const [names, setNames] = useState([]);
    const [nbLatence, setLatence] = useState([]);
    const [colors, setColors] = useState([]);

    const style = {
        // "width": "35rem"
        textAlign : "center",
        color: "#494848",
    }
   

    useEffect(() => {
        props.setLatencyLoading(true)
        axios.get('http://localhost:8000/data/sub_categories', {
            params: {
                date: dateLog
            },
            headers: {
                Authorization: "Bearer " + token,
            },
        })
            .then(async (resp) => {
                let couleurs = [];
                for (let i = 0; i < resp.data.sub_cat.length; i++) {
                    const element = resp.data.sub_cat[i];
                    const latence = nbLatence;
                    const nameSub_category = names;
                    latence.push(element.latence);
                    nameSub_category.push(element._id)
                    if (element.latence <= 1000){
                        couleurs.push("#00D100");
                    }
                    else if (element.latence > 1000 && element.latence < 2000){
                        couleurs.push("#ff8c00");     
                    }
                    else if (element.latence >= 2000){
                        couleurs.push("#d40000");
                    }
                    setColors(couleurs);
                    setLatence(latence);
                    setNames(nameSub_category);
                    props.setLatencyLoading(false)
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])




    return (
        <>
        <h5 style={style}>Temps de Latence par Sous-Catégorie</h5>
            <CChart
                style={style}
                type="bar"
                data={{
                    labels: names,
                    datasets: [
                        {
                            label: 'Moyenne de Temps de Latence (ms)',
                            backgroundColor: colors,
                            data: nbLatence,
                        },
                    ],
                }}
                labels="months"
            />
        </>
    )
}