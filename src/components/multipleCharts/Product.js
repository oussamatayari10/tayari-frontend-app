import { CChart } from '@coreui/react-chartjs';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
/* eslint-disable react/prop-types */

const token = localStorage.getItem('token');

export default function Product(props) {
    const { dateLog } = props;

    const [count, setCount] = useState([]);
    const [products, setProducts] = useState([]);
    const [colors, setColors] = useState([]);

    const style = {
        // "width": "35rem"
        textAlign: "center",
        color: "#494848",
    }


    useEffect(() => {
        props.setProductLoading(true);
        axios.get('http://localhost:8000/sub_category', {
            params: {
                date: dateLog,
                sub_category: "purchaseBundle"
            },
            headers: {
                Authorization: "Bearer " + token,
            },
        })
            .then(async (resp) => {
                setProducts(resp.data.productsId.id);
                setCount(resp.data.productsId.count)
                let couleurs = [];
                for (let i = 0; i < resp.data.productsId.id.length; i++) {                   
                    couleurs.push("#" + Math.floor(Math.random() * 16777215).toString(16))
                    setColors(couleurs);
                }
                props.setProductLoading(false)
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])




    return (
        <>
            {/* <h5 style={style}>Temps de Latence par Sous-Catégorie</h5> */}
            <CChart
                style={style}
                type="bar"
                data={{
                    labels: products,
                    datasets: [
                        {
                            label: 'Nombre de Transactions par produit',
                            backgroundColor: colors,
                            data: count,
                        },
                    ],
                }}
            />
        </>
    )
}