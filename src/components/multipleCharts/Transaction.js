/* eslint-disable react/prop-types */
import { CChart } from '@coreui/react-chartjs';
import axios from 'axios';
import React, { useEffect, useState } from 'react';

const token = localStorage.getItem('token');

export default function Transaction(props) {

    const { dateLog } = props;
    const [nbTransaction, setNbTransaction] = useState([]);
    const [names, setNames] = useState([]);
    const [colors, setColors] = useState([]);

    const style = {
        // "width": "35rem"
        textAlign: "center",
        color: "#494848",
    }

    useEffect(() => {
        props.setTransactionLoading(true)
        axios.get(`http://localhost:8000/data/sub_categories`, {
            params: {
                date: dateLog
            },
            headers: {
                Authorization: "Bearer " + token,
            },
        })
            .then(async (resp) => {
                let couleurs = [];
                for (let i = 0; i < resp.data.sub_cat.length; i++) {
                    const element = resp.data.sub_cat[i];
                    const nbTrans = nbTransaction;
                    const nameSub_category = names;
                    nbTrans.push(element.count);
                    nameSub_category.push(element._id)
                    setNbTransaction(nbTrans);
                    setNames(nameSub_category);
                    couleurs.push("#" + Math.floor(Math.random() * 16777215).toString(16))
                    setColors(couleurs)
                }
                props.setTransactionLoading(false)
            })
            .catch((err) => {
                console.log(err);
                props.setTransactionLoading(false)
            })

    }, []);

    return (
        <>
            <h5 style={style}>Nombre de Transactions par Sous-Catégorie</h5>
            <CChart
                style={style}
                type="doughnut"
                data={{
                    labels: names,
                    datasets: [
                        {
                            backgroundColor: colors,
                            data: nbTransaction,
                        },
                    ],
                }}
            />
        </>
    )
}