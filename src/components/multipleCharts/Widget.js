/* eslint-disable react/prop-types */
import { CCol, CRow, CWidgetStatsB } from '@coreui/react';
import axios from 'axios';
import React, { useEffect, useState } from 'react';


const token = localStorage.getItem('token');

const Widget = (props) => {
    const { dateLog , nameSubCategory} = props;
    const [nbClients, setNbClients] = useState(0);
    const [nbSuccess, setNbSuccess] = useState(0);
    const [nbTransaction, setNbTransaction] = useState(0);
    const [latence, setLatence] = useState(0);

    useEffect(() => {
        props.setWidgetLoading(true)
        axios.get('http://localhost:8000/sub_category', {
            params: {
                date: dateLog,
                sub_category: nameSubCategory
            },
            headers: {
                Authorization: "Bearer " + token,
            },
        })
            .then(async (resp) => {
                console.log(resp);
                setNbClients(resp.data.nbClient);
                setLatence(resp.data.latence);
                setNbTransaction(resp.data.nbTransaction);
                setNbSuccess(resp.data.success)
                props.setWidgetLoading(false)
            })
            .catch((err) => {
                console.log(err.message);
            })
    }, [])

    return (
        <>
            <CRow>
                <CCol xs={6}>
                    <CWidgetStatsB
                        className="mb-3"
                        color="danger"
                        inverse
                        progress={{ value:nbClients }}
                        text="Nombre des Utilisateurs"
                        title="Clients"
                        value={nbClients}
                    />
                </CCol>
                <CCol xs={6}>
                    <CWidgetStatsB
                        className="mb-3"
                        color="info"
                        inverse
                        progress={{ value:latence }}
                        text="Temps de Latence Moyenne"
                        title="Latence (s)"
                        value={latence}
                    />
                </CCol>
            </CRow>
            <CRow >
                <CCol xs={6}>
                    <CWidgetStatsB
                        className="mb-3"
                        color="success"
                        inverse
                        progress={{ value:nbSuccess }}
                        text="Pourcentage des transactions réussites"
                        title="Success (%)"
                        value={nbSuccess}
                    />
                </CCol>
                <CCol xs={6}>
                    <CWidgetStatsB
                        className="mb-3"
                        color="primary"
                        inverse
                        progress={{ value:nbTransaction }}
                        text="Nombre de Transactions Effectuées"
                        title="Transactions"
                        value={nbTransaction}
                    />
                </CCol>
            </CRow>
        </>
    )
}
export default Widget