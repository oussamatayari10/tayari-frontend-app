import React from 'react'
import { AppContent, AppSidebar, AppFooter, AppHeader } from '../components/index'

const isAuth = () => {
  if (localStorage.getItem("token")) return true
  else return false;
}

const DefaultLayout = () => {
  return (
        <>
      {isAuth() ? (
        <div>
          <AppSidebar />
          <div className="wrapper d-flex flex-column min-vh-100 bg-light">
            <AppHeader />
            <div className="body flex-grow-1 px-3">
              <AppContent />
            </div>
            <AppFooter />
          </div>
        </div>
      ) : (
        <>
          {window.location.assign('/#/login')}
        </>
      )};
      </>
     
  )
}

export default DefaultLayout
