import React from 'react'

const RechargeHRN = React.lazy(() => import('./views/rechargeHRN/RechargeHRN'))
const GetSubscriberInfo = React.lazy(() => import('./views/getSubscriberInfo/GetSubscriberInfo'))
const PurchaseBundle = React.lazy(() => import('./views/purchaseBundle/PurchaseBundle'))
const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))
const Logs = React.lazy(() => import('./views/addLogs/AddLogs'))
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', element: Dashboard },
  { path: '/purchaseBundle', name: 'purchaseBundle', element: PurchaseBundle },
  { path: '/rechargeHRN', name: 'RechargeHRN', element: RechargeHRN },
  { path: '/getSubscriberInfo', name: 'GetSubscriberInfo', element: GetSubscriberInfo },
  { path: '/logs', name: 'Logs', element: Logs },
]

export default routes
