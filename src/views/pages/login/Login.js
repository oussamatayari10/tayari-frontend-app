import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import {
  CAlert,
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import axios from 'axios'
import { setAuthToken } from 'src/components/setAuthToken'

const style = {
  color: 'white'
};


const Login = () => {
  const defaultAuth = {
    email: "",
    password: ""
  }

  const [error, setError] = useState([])
  const [visible, setVisible] = useState(false)
  const [authen, setAuthen] = useState(defaultAuth)

  //error handle
  const errorHandler = (err) => {
    let error = err.response.data.code;
    let errMsg = "";
    switch (error) {
      case '0': errMsg = "Email non conforme";
        break;
      case '1': errMsg = "Utilisateur introuvable";
        break;
      case '2': errMsg = "Mot de passe Incorrect. Réessayez une autre fois";
        break;
      default: errMsg = ""
    }
    return errMsg;
  }

  //handle change
  const handleChange = (e) => {
    const { name, value } = e.target;
    setAuthen({
      ...authen,
      [name]: value,
    })
  }

  //submit handler
  const submitHandler = (e) => {
    e.preventDefault();
    const authentificated = {
      email: authen.email,
      password: authen.password
    }

    axios.post('http://localhost:8000/api/signin', authentificated)
      .then(res => {
        console.log(res);
        const token = res.data.token;
        const role = res.data.user.role;
        const name = res.data.user.name;
        const username = res.data.user.username;
        localStorage.setItem("token", token);
        localStorage.setItem("role", role);
        localStorage.setItem("name", username + " "+name);
        setAuthToken(token);
        window.location.assign('/')
        console.log('authetificated user');
      })
      .catch(err => {
        console.log(err);
        setVisible(true)
        let errMsg = errorHandler(err);
        console.log(errMsg);
        setError(errMsg)
      })
  }

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CAlert visible={visible} color="danger">
          <svg className="flex-shrink-0 me-2" width="24" height="100%" viewBox="0 0 512 512">
            <rect width="32" height="176" x="240" y="176" fill="var(--ci-primary-color, currentColor)" className="ci-primary"></rect><rect width="32" height="32" x="240" y="384" fill="var(--ci-primary-color, currentColor)" className="ci-primary"></rect><path fill="var(--ci-primary-color, currentColor)" d="M274.014,16H237.986L16,445.174V496H496V445.174ZM464,464H48V452.959L256,50.826,464,452.959Z" className="ci-primary"></path>
          </svg>
          <span>
            {error}
          </span>
        </CAlert>
        <CRow className="justify-content-center">
          <CCol md={8}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={submitHandler}>
                    <h1>Connexion</h1>
                    <p className="text-medium-emphasis">Connectez vous à votre Compte</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput
                        type='email'
                        name='email'
                        placeholder="Email"
                        value={authen.email}
                        onChange={handleChange}
                        autoComplete="email"
                        required />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput
                        type="password"
                        name='password'
                        placeholder="Mot de passe"
                        value={authen.password}
                        onChange={handleChange}
                        autoComplete="mot de passe actuel"
                        required
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                        <CButton color="danger" className="px-4" type='submit'>
                          <span style={style}>Se connecter</span>
                        </CButton>
                      </CCol>
                      <CCol xs={6} className="text-right">
                        <CButton color="link" className="px-0" >
                          Mot de passe oublié ?
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard className="text-white bg-danger py-5" style={{ width: '44%' }}>
                <CCardBody className="text-center">
               <div>
                    <h2>S&apos;inscrire</h2>
                    <p>
                      Si vous n&apos;avez pas encore un compte, n&apos;hésitez pas de vous inscrire
                    </p>
                    <Link to="/register">
                      <CButton color="light" variant="outline" className="mt-4" tabIndex={-1}>
                        S&apos;inscrire maintenant
                      </CButton>
                    </Link>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
