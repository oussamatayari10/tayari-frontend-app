import React, { useState } from 'react'
import axios from "axios"
import {
  CAlert,
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilContact, cilLockLocked, cilUser } from '@coreui/icons'
import { Link } from 'react-router-dom'

const style = {
  padding: '0'
}
const stylebtn = {
  color: "white"
}
const styleh1 = {
  color: "#d40000"
}
const setAuthToken = token => {
  if (token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  }
  else
    delete axios.defaults.headers.common["Authorization"];
}
const Register = () => {
  const defaultUser = {
    name: "",
    username: "",
    email: "",
    password: "",
    password_confirmation: "",
    role: ""
  }

  const [user, setUser] = useState(defaultUser)
  const [error, setError] = useState([])
  const [visible, setVisible] = useState(false)

  //handle Chande
  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser({
      ...user,
      [name]: value,
    })
  }

  const errorHandler = (err) => {
    let error = err.response.data.code;
    let errMsg = "";
    switch (error) {
      case '0': errMsg = "Email déja existant. Veuillez se connecter à votre compte";
        break;
      case '1': errMsg = "Email non conforme";
        break;
      case '2': errMsg = "Mot de passe Invalide. Réessayez une autre fois";
        break;
      case '3': errMsg = "Nombre de caractèrs inférieur à 8";
        break;
      case '4': errMsg = "Quelque Chose ne marche pas";
        break;
      default: errMsg = ""
    }
    return errMsg;
  }

  //register function
  const handleSubmit = (event) => {
    event.preventDefault();
    const userData = {
      name: user.name,
      username: user.username,
      email: user.email,
      password: user.password,
      password_confirmation: user.password_confirmation,
      role: user.role
    };
    axios.post('http://localhost:8000/api/signup', userData)
      .then(response => {
        const token = response.data.token;
        localStorage.setItem("token", token);
        localStorage.setItem("role", userData.role);
        localStorage.setItem("name", userData.username + " " +userData.name);
        setAuthToken(token);
        window.location.assign('/')
      })
      .catch(err => {
        console.log(err);
        setVisible(true)
        let errMsg = errorHandler(err);
        console.log(errMsg);
        setError(errMsg)
      });
  }

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CAlert visible={visible} color="danger">
          <svg className="flex-shrink-0 me-2" width="24" height="100%" viewBox="0 0 512 512">
            <rect width="32" height="176" x="240" y="176" fill="var(--ci-primary-color, currentColor)" className="ci-primary"></rect><rect width="32" height="32" x="240" y="384" fill="var(--ci-primary-color, currentColor)" className="ci-primary"></rect><path fill="var(--ci-primary-color, currentColor)" d="M274.014,16H237.986L16,445.174V496H496V445.174ZM464,464H48V452.959L256,50.826,464,452.959Z" className="ci-primary"></path>
          </svg>
          <span>
            {error}
          </span>
        </CAlert>
        <CRow className="justify-content-center">
          <CCol md={9} lg={7} xl={6}>
            <CCard className="mx-4">
              <CCardBody className="p-4">
                <CForm onSubmit={handleSubmit}>
                  <h1 style={styleh1}>S&apos;inscrire</h1>
                  <p className="text-medium-emphasis">Créer votre Compte</p>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput
                      placeholder="Nom"
                      name="name"
                      value={user.name}
                      onChange={handleChange}
                      autoComplete="nom"
                      required
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput
                      placeholder="Prenom"
                      name="username"
                      value={user.username}
                      onChange={handleChange}
                      required
                      autoComplete="prenom" />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>@</CInputGroupText>
                    <CFormInput
                      placeholder="Email"
                      name="email"
                      type='email'
                      value={user.email}
                      required
                      onChange={handleChange}
                      autoComplete="email" />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      name="password"
                      required
                      placeholder="Mot de Passe"
                      value={user.password}
                      onChange={handleChange}
                      autoComplete="new-password"
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      name="password_confirmation"
                      required
                      value={user.password_confirmation}
                      onChange={handleChange}
                      placeholder="Répétez Mot de passe"
                      autoComplete="new-password"
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilContact} />
                    </CInputGroupText>
                    <CFormSelect 
                      required
                      name='role'
                      value={user.role}
                      onChange={handleChange}
                      options={[
                        'Choisissez votre rôle',
                        { label: 'Digital IT', value: '1' },
                        { label: 'Autres', value: '2' },
                      ]}
                      />
                    
                  </CInputGroup>
                  <div className="d-grid">
                    <CButton color="danger" type='submit' style={stylebtn}><span >Créer un Compte </span></CButton>
                  </div>
                </CForm>
                <span className="justify-center text-sm text-center text-gray-500 flex-items-center dark:text-gray-400">
                  Vous avez déjà un Compte ?&nbsp;&nbsp;
                  <Link to="/login">
                    <CButton color="link" style={style}>
                      Se connecter
                    </CButton>
                  </Link>
                </span>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Register;
