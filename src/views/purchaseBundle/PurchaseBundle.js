import React, { useState } from 'react'
import { CButton, CCard, CCardBody, CCol, CForm, CFormInput, CModal, CRow } from '@coreui/react'
import queryString from 'query-string';
import Widget from 'src/components/multipleCharts/Widget';
import Product from 'src/components/multipleCharts/Product';
// import './Dashboard.css'
const PurchaseBundle = () => {

    const [ProductLoading, setProductLoading] = useState(false);
    const [WidgetLoading, setWidgetLoading] = useState(false);

    let dateNow = new Date()
    let params = queryString.parse(window.location.search)
    const dateYesterday = dateNow.getDate() - 1;
    dateNow.setDate(dateYesterday)
    const [dateLog, setDatelog] = useState(dateNow.toISOString().split('T')[0]);
    let dateLoaded = undefined;
    const handleChange = (e) => {
        const { name, value } = e.target;
        setDatelog({
            [name]: value,
        })
    }

    const submitHandler = (e) => {

    }

    const submitBackHandler = (e) => {
        e.preventDefault();
        window.location.assign('/#/purchaseBundle')
    }
    params.dateDay === undefined ? dateLoaded = dateLog : dateLoaded = params.dateDay
    return (
        <>
            <div className='hstyle'><span>Les Statistiques de PurchaseBundle pour {dateLoaded.toString()}</span></div>
            {/* <CModal
                visible={ProductLoading || WidgetLoading}
            >
                <div className="container">
                    <div className="spinner"></div>
                </div>
            </CModal> */}
            <CCard className="bg-white py-0 mb-2" xs={6}>
                <CCardBody className="">
                    <CRow>
                        <CCol xs={8}>
                            <CForm onSubmit={submitHandler} className='mb-4'>
                                <p className="text-medium-emphasis">Sélectionner la date des logs à consulter</p>
                                <CRow>
                                    <CCol xs={3} className="text-right">
                                        <CFormInput
                                            type='date'
                                            name='dateDay'
                                            onChange={handleChange}
                                            required />
                                    </CCol>
                                    <CCol xs={6} className="text-right">
                                        <CButton color="danger" className="px-4 text-white" type='submit' >
                                            Sélectionner date
                                        </CButton>
                                    </CCol>
                                </CRow>
                            </CForm>
                        </CCol>
                        <CCol xs={4} className="text-center">
                            <CForm onSubmit={submitBackHandler} className='mb-4'>
                                <p className="text-medium-emphasis">Retourner aux statistiques de {dateLoaded.toString()}</p>
                                <CButton color="danger" className="px-4 text-white" type='submit' >
                                    Retourner
                                </CButton></CForm>
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
            <CCard className="mb-2">
                <CCardBody>
                    <Widget dateLog={dateLoaded} nameSubCategory="purchaseBundle" setWidgetLoading={setWidgetLoading}/>
                </CCardBody>
            </CCard>
            <CCard className="mb-2">
                <CCardBody>
                    <Product dateLog={dateLoaded} setProductLoading={setProductLoading}/>
                </CCardBody>
            </CCard>
        </>
    )
}

export default PurchaseBundle
