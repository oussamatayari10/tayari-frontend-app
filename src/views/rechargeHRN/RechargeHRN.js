import React, { useState } from 'react'
import { CButton, CCard, CCardBody, CCol, CForm, CFormInput, CModal, CRow } from '@coreui/react'
import queryString from 'query-string';
import Widget from 'src/components/multipleCharts/Widget';
// import './Dashboard.css'
const RechargeHRN = () => {

    const [WidgetLoading, setWidgetLoading] = useState(false);

    let dateNow = new Date()
    let params = queryString.parse(window.location.search)
    const dateYesterday = dateNow.getDate() - 1;
    dateNow.setDate(dateYesterday)
    const [dateLog, setDatelog] = useState(dateNow.toISOString().split('T')[0]);
    let dateLoaded = undefined;
    const handleChange = (e) => {
        const { name, value } = e.target;
        setDatelog({
            [name]: value,
        })
    }

    const submitHandler = (e) => {

    }
    const submitBackHandler = (e) => {
        e.preventDefault();
        window.location.assign('/#/dashboard')
    }
    params.dateDay === undefined ? dateLoaded = dateLog : dateLoaded = params.dateDay
    return (
        <>
            <div className='hstyle'><span>Les Statistiques de RechargeHRN pour {dateLoaded.toString()}</span></div>
            {/* <CModal
                visible={WidgetLoading}
            >
                <div className="container">
                    <div className="spinner"></div>
                </div>
            </CModal> */}
            <CCard className="bg-white py-0 mb-2" xs={6}>
                <CCardBody className="">
                    <CRow>
                        <CCol xs={8}>
                            <CForm onSubmit={submitHandler} className='mb-4'>
                                <p className="text-medium-emphasis">Sélectionner la date des logs à consulter</p>
                                <CRow>
                                    <CCol xs={3} className="text-right">
                                        <CFormInput
                                            type='date'
                                            name='dateDay'
                                            onChange={handleChange}
                                            required />
                                    </CCol>
                                    <CCol xs={6} className="text-right">
                                        <CButton color="danger" className="px-4 text-white" type='submit' >
                                            Sélectionner date
                                        </CButton>
                                    </CCol>
                                </CRow>
                            </CForm>
                        </CCol>
                        <CCol xs={4} className="text-center">
                            <CForm onSubmit={submitBackHandler} className='mb-4'>
                                <p className="text-medium-emphasis">Retourner aux statistiques de {dateLoaded.toString()}</p>
                                <CButton color="danger" className="px-4 text-white" type='submit' >
                                    Retourner
                                </CButton></CForm>
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
            <CCard className="mb-2">
                <CCardBody>
                    <Widget dateLog={dateLoaded} nameSubCategory="rechargeHRN" setWidgetLoading={setWidgetLoading}/>
                </CCardBody>
            </CCard>
        </>
    )
}

export default RechargeHRN
